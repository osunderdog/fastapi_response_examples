# fastapi_response_examples

Work to demonstrate different response data structures.

Want to measure the performance of several response models to see which of them provides
the best performance.  Want to find the best response for pandas data frame in 'records' format.

Want to be able to demonstrate different size and shape of pandas data frames to see which response
model seems to work best overall.