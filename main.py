from fastapi import FastAPI
from fastapi.responses import JSONResponse, ORJSONResponse, Response, UJSONResponse
import uvicorn
from rand_data_frame import RandDataFrame
import json
import bson
import msgpack
from typing import Any

app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Hello World"}

# TODO: Consider construction of the default array size.  How much time is it taking to create the data frame

class BSONResponse(JSONResponse):
    def render(self, content: Any) -> bytes:
        assert bson is not None, "bson must be installed to use BSONResponse"
        return bson.dumps({'data': content})

class MSGPACKResponse(JSONResponse):
    def render(self, content: Any) -> bytes:
        assert msgpack is not None, "ujson must be installed to use UJSONResponse"
        return msgpack.dumps(content)


@app.get("/bson/multi/{size}")
async def bson_multi(size: int):
    return BSONResponse(content=RandDataFrame().get(rows=size).to_dict(orient='records'))

@app.get("/orjson/multi/{size}")
async def orjson_multi(size: int):
    return ORJSONResponse(content=RandDataFrame().get(rows=size).to_dict(orient='records'))

@app.get("/json/multi/{size}")
async def json_multi(size: int):
    return JSONResponse(content=RandDataFrame().get(rows=size).to_dict(orient='records'))

@app.get("/msgpack/multi/{size}")
async def msgpack_multi(size: int):
    return MSGPACKResponse(content=RandDataFrame().get(rows=size).to_dict(orient='records'))

if __name__ == '__main__':
    uvicorn.run(app="main:app", reload=True, port=8088)
