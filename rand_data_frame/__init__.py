from random import shuffle, choices, uniform
import pandas as pd
from pydantic import BaseModel
from typing import List, ClassVar

class RandDataFrame(BaseModel):
    CategoryList: ClassVar[List[str]] = ['red', 'green', 'blue', 'yellow']

    @classmethod
    def rand_type_list(cls, rows: int) -> List[str]:
        return choices(population=cls.CategoryList, k=rows)

    @classmethod
    def rand_int_list(cls, rows: int) -> List[str]:
        data = list(range(rows))
        shuffle(data)
        return data

    @classmethod
    def rand_float_list(cls, rows: int) -> List[float]:
        return [uniform(-5.0, 5.0) for _ in range(rows)]

    @classmethod
    def index_list(cls, rows: int) -> List[float]:
        return list(range(rows))

    def get(self, rows: int) -> pd.DataFrame:
        return pd.DataFrame.from_dict({'index': self.index_list(rows=rows),
                                       'rint': self.rand_int_list(rows=rows),
                                       'rcat': self.rand_type_list(rows=rows),
                                       'rfloat': self.rand_float_list(rows=rows)})
